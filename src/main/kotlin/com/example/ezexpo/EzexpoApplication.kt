package com.example.ezexpo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EzexpoApplication

fun main(args: Array<String>) {
    runApplication<EzexpoApplication>(*args)
}
