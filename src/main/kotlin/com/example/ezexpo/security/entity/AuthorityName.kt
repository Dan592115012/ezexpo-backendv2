package com.example.ezexpo.security.entity

enum class AuthorityName {
    ROLE_BOOTHOWNER,ROLE_EVENTORGANIZER
}