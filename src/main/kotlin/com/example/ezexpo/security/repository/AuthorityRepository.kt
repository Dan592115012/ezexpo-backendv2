package com.example.ezexpo.security.repository

import com.example.ezexpo.security.entity.Authority
import com.example.ezexpo.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository : CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}