package com.example.ezexpo.controller

import com.example.ezexpo.entity.BoothOwner
import com.example.ezexpo.entity.User
import com.example.ezexpo.entity.UserRole
import com.example.ezexpo.entity.dto.UserDto
import com.example.ezexpo.entity.dto.displayUser
import com.example.ezexpo.service.BoothOwnerService
import com.example.ezexpo.service.UserService
import com.example.ezexpo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import kotlin.Enum as Enum1


@RestController

class UserController {
    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var boothOwnerService: BoothOwnerService

    @PostMapping("/login")
    fun loginByUserId(@RequestParam("username")username:String, @RequestParam("password")password:String):ResponseEntity<Any>{
        try{
            val output = MapperUtil.INSTANCE.mapUserDto(userService.getUserByUsernameAndPassword(username, password))
            output?.let { return ResponseEntity.ok(it) }
        }catch (n: KotlinNullPointerException) {
            return ResponseEntity.ok("false")
        }
        return ResponseEntity.ok("false")
    }

    @PutMapping("/updateuser/{userid}")
    fun updateUser(@PathVariable("userid")id:String?,
                   @RequestBody user: displayUser):ResponseEntity<Any>{
        user.id=id?.toLong()!!
        if (id == null){
            return ResponseEntity.ok("null")
        }else{

            try {
                val output = MapperUtil.INSTANCE.mapUserDto(userService.update(user!!))
                output?.let{ return ResponseEntity.ok(it)}
                return ResponseEntity.ok("not found ")
            }catch (e:KotlinNullPointerException){
                return ResponseEntity.ok("not found ")
            }catch (n:NumberFormatException){
                return ResponseEntity.ok("invalid format")
            }
        }
    }

    @GetMapping("/forgotPassword")
    fun getUserByEmail(@RequestParam("email")email:String):ResponseEntity<Any>{

            try{
                val output = MapperUtil.INSTANCE.mapUserDto(userService.getUserByEmail(email))
                output?.let {return ResponseEntity.ok(it)}
                return ResponseEntity.ok("not found")
            }catch (n: KotlinNullPointerException) {
                return ResponseEntity.ok("invalid format")
            }



    }
    @PostMapping("/newBoothOwner")
    fun addBoothOwner(@RequestBody user: displayUser): ResponseEntity<Any> {
        try{
            val output = MapperUtil.INSTANCE.mapUserDto(userService.save(user))
            output?.let {
                if ( it.username == null || it.email == null || it.password == null || it.firstName == null || it.lastName == null){
                    return ResponseEntity.ok("not input all required information")
                }
                return ResponseEntity.ok(it) }
        } catch (n: KotlinNullPointerException) {
             return ResponseEntity.ok("false")
        }


        return ResponseEntity.ok("false")
    }

    @GetMapping("/User")
    fun getUserByRole(@RequestParam("role")role:String):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapUserDto(userService.getUserByRole(role))
        output?.let{return ResponseEntity.ok(it)}
    }

    @GetMapping("/UserById")
    fun getUserById(@RequestParam("id")id: String): ResponseEntity<Any>{
        try {
            val output = MapperUtil.INSTANCE.mapUserDto(userService.getUserById(id.toLong())!!)
            output?.let{return ResponseEntity.ok(it)}
            return ResponseEntity.ok("false")
        }catch (e:NumberFormatException){
            return ResponseEntity.ok("invalid format")
        }catch (n: KotlinNullPointerException) {
            return ResponseEntity.ok("not found")
        }

    }

}




