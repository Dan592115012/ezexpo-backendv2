package com.example.ezexpo.controller

import com.example.ezexpo.service.BoothOwnerService
import com.example.ezexpo.service.UserService
import com.example.ezexpo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController

class BoothOwnerController {
    @Autowired
    lateinit var boothOwnerService: BoothOwnerService
    @Autowired
    lateinit var userService: UserService


    @GetMapping("/booth/userId")
    fun getBoothDetailByUserId(@RequestParam("id") id: String): ResponseEntity<Any> {

        try {
            val output = MapperUtil.INSTANCE.mapBoothOwnerDto(boothOwnerService.getBoothDetailById(id.toLong())!!)
            output?.let { return ResponseEntity.ok(it) }
        } catch (n: KotlinNullPointerException) {
            return ResponseEntity.ok("false")
        } catch (e: NumberFormatException) {
            return ResponseEntity.ok("false")
        }

        return ResponseEntity.ok("false")
    }
}