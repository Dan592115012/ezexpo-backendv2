package com.example.ezexpo.controller

import com.example.ezexpo.entity.SurveyQuestion
import com.example.ezexpo.service.BoothOwnerService
import com.example.ezexpo.service.SurveyQuestionService
import com.example.ezexpo.service.UserService
import com.example.ezexpo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.NumberFormatException


@RestController
class EventOrganizerController {
    @Autowired
    lateinit var boothOwnerService: BoothOwnerService

    @Autowired
    lateinit var surveyQuestionService: SurveyQuestionService

    @GetMapping("/booth")
    fun getAllBoothDetail():ResponseEntity<Any>{
        val output = boothOwnerService.getAllBoothDetail()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapBoothOwnerDto(output))
    }

    @GetMapping("/survey")
    fun getAllSurvey():ResponseEntity<Any>{
        val output = surveyQuestionService.getAllSurvey()
        return ResponseEntity.ok(output)
    }

    @GetMapping("/survey/id")
    fun getSurveyById(@RequestParam("id")id:String):ResponseEntity<Any>{
        try{
            val output = surveyQuestionService.getSurveybyId(id.toLong()!!)
            output?.let{return ResponseEntity.ok(it)}
            return ResponseEntity.ok("not found")
        }catch (e:KotlinNullPointerException){
            return ResponseEntity.ok("not found")
        }catch (n:NumberFormatException){
            return ResponseEntity.ok("invalid format")
        }

    }

    @PostMapping("/createSurvey")
    fun createNewSurvey(@RequestBody survey : SurveyQuestion):ResponseEntity<Any>{
        try{
            val output = surveyQuestionService.save(survey)
            output?.let{
                if(it.surveyName == null || it.surveyDescription == null || it.question1 == null || it.question2 == null || it.question3 == null || it.question4 == null || it.question5 == null){
                    return ResponseEntity.ok("not input all required information")
                }
                return ResponseEntity.ok(it)}

        }catch (e: KotlinNullPointerException){
            return ResponseEntity.ok("false")
        }
    }










}