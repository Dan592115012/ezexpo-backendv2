package com.example.ezexpo.entity

import javax.persistence.*


@Entity
data class EventOrganizer(override var firstName: String? = null,
                          override var lastName: String? = null,
                          override var email: String? = null,
                          override var username: String? = null,
                          override var password: String? = null,
                          override var userRole: UserRole? = UserRole.EVENTORGANIZER
                        ): User (firstName, lastName , email,username,password,userRole) {
    @OneToMany
    var surveyQuestion : MutableList<SurveyQuestion>? = mutableListOf()
}
