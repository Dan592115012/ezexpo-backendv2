package com.example.ezexpo.entity

enum class UserRole {
    EVENTORGANIZER,BOOTHOWNER
}