package com.example.ezexpo.entity
import javax.persistence.*


@Entity

data class SurveyAnswer(var answer1:Int?=null,
                        var answer2:Int?=null,
                        var answer3:Int?=null,
                        var answer4:Int?=null,
                        var answer5:Int?=null,
                        var sugesstion:String?=null) {
    @Id
    @GeneratedValue
    var id:Long?=null

}