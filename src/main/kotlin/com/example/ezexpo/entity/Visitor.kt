package com.example.ezexpo.entity

import javax.persistence.*

@Entity
 data class Visitor ( var happy : Int? = null,
                      var sad : Int? = null,
                      var surprise : Int? = null,
                      var angry : Int ? = null,
                      var disappoint : Int? = null,
                      var fear : Int? = null,
                      var netural:Int? = null,
                      var numOfMale : Int? = null,
                      var numOfFemale : Int? = null,
                      var visitTime : Long? = null

 ) {
 @Id
 @GeneratedValue
 var id:Long?=null



}