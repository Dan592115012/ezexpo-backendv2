package com.example.ezexpo.entity

import javax.persistence.*

@Entity
data class BoothOwner (override var firstName: String? = null,
                       override var lastName: String? = null,
                       override var email: String? = null,
                       override var username: String? = null,
                       override var password: String? = null,
                       override var userRole: UserRole? = com.example.ezexpo.entity.UserRole.BOOTHOWNER,
                       var boothName : String? = null

 ) : User (firstName, lastName , email,username,password,userRole) {

@OneToMany
  var visitor: MutableList<Visitor>? = mutableListOf<Visitor>()

}