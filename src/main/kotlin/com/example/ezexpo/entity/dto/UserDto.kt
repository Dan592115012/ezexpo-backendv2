package com.example.ezexpo.entity.dto

import com.example.ezexpo.entity.UserRole

class UserDto (var firstName:String? = null,
               var lastName:String? = null,
               var email:String? = null,
               var username:String? = null,
               var password:String? = null,
               var UserRole: UserRole? = null,
               var id:Long? = null,
               var authorities:List<AuthorityDto> = mutableListOf())
//               ,
//               var Visitor:List<VisitorDto>? = null)

class displayUser (var firstName:String? = null,
                   var lastName:String? = null,
                   var email:String? = null,
                   var username:String? = null,
                   var password:String? = null,
                   var UserRole: UserRole? = null,
//                   var Visitor:MutableList<VisitorDto>? = null,
                   var id:Long? = null)
