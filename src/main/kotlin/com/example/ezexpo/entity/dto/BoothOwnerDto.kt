package com.example.ezexpo.entity.dto

import com.example.ezexpo.entity.UserRole

class BoothOwnerDto (var firstName:String? = null,
                     var lastName:String? = null,
                     var email:String? = null,
                     var username:String? = null,
                     var UserRole: UserRole? = null,
                     var boothName : String? = null,
                     var id:Long? = null,
                     var Visitor:List<VisitorDto>? = null)

class BoothOwnerDtoNoVisitor (var firstName:String? = null,
                     var lastName:String? = null,
                     var email:String? = null,
                     var username:String? = null,
                     var password:String? = null,
                     var UserRole: UserRole? = null,
                     var boothName : String? = null,
                     var id:Long? = null)