package com.example.ezexpo.dao

import com.example.ezexpo.entity.SurveyQuestion
import com.example.ezexpo.repositoy.SurveyQuestionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class SurveyQuestionDaoImpl : SurveyQuestionDao{


    @Autowired
    lateinit var surveyQuestionRepository: SurveyQuestionRepository

    override fun getAllSurvey(): List<SurveyQuestion> {
        return surveyQuestionRepository.findAll().filterIsInstance(SurveyQuestion::class.java)
    }
    override fun getSurveyById(id: Long): SurveyQuestion?{
       return surveyQuestionRepository.findById(id).orElse(null)
    }

    override fun save(survey: SurveyQuestion): SurveyQuestion {

        return surveyQuestionRepository.save(survey)
    }

}