package com.example.ezexpo.dao

import com.example.ezexpo.entity.SurveyQuestion

interface SurveyQuestionDao {
    fun getAllSurvey(): List<SurveyQuestion>
    fun getSurveyById(id: Long): SurveyQuestion?
    fun save(survey: SurveyQuestion): SurveyQuestion

}