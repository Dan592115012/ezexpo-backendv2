package com.example.ezexpo.dao

import com.example.ezexpo.entity.SurveyAnswer

interface SurveyAnswerDao {
    fun save(answer: SurveyAnswer): SurveyAnswer
}