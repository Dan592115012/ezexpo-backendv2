package com.example.ezexpo.dao

import com.example.ezexpo.entity.BoothOwner
import com.example.ezexpo.repositoy.BoothOwnerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class BoothOwnerDaoImpl:BoothOwnerDao {
    override fun getBoothDetailById(id: Long): BoothOwner? {
       return boothOwnerRepository.findById(id).orElse(null)
    }

    override fun getBoothDetailByName(boothName: String): BoothOwner? {
        return boothOwnerRepository.findByBoothName(boothName)
    }

    override fun getAllBoothDetail(): List<BoothOwner> {
        return boothOwnerRepository.findAll().filterIsInstance(BoothOwner::class.java)
    }

//    override fun getBoothDetailByName(boothName: String): BoothOwner? {
//        return boothOwnerRepository.findByName(boothName)
//    }


    @Autowired
    lateinit var boothOwnerRepository: BoothOwnerRepository

}