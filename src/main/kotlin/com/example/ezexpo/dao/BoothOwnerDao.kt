package com.example.ezexpo.dao

import com.example.ezexpo.entity.BoothOwner

interface BoothOwnerDao {
    //fun getBoothDetailByName(boothName: String): BoothOwner?
    fun getAllBoothDetail(): List<BoothOwner>

    fun getBoothDetailByName(boothName: String): BoothOwner?
    fun getBoothDetailById(id: Long): BoothOwner?
}