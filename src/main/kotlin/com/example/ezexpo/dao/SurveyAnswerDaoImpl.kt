package com.example.ezexpo.dao

import com.example.ezexpo.entity.SurveyAnswer
import com.example.ezexpo.repositoy.SurveyAnswerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class SurveyAnswerDaoImpl : SurveyAnswerDao{

    @Autowired
    lateinit var surveyAnswerRepository: SurveyAnswerRepository

    override fun save(answer: SurveyAnswer): SurveyAnswer {
       return surveyAnswerRepository.save(answer)
    }
}