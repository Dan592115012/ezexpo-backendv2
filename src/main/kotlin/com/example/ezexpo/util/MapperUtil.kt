package com.example.ezexpo.util

import org.mapstruct.*
import org.mapstruct.factory.Mappers
import com.example.ezexpo.entity.*
import com.example.ezexpo.entity.dto.*

@Mapper(componentModel = "spring")

interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

//    @Mappings(
//            Mapping(source = "user.username",target = "username"),
//            Mapping(source= "user.authorities",target = "authorities")
//    )


    @InheritInverseConfiguration
    fun mapUserDto(user:displayUser):User

    fun mapUserDto(user: User?):UserDto?
    fun mapUserDto(users:List<User>):List<UserDto>

    fun mapBoothOwnerDto(boothOwner: BoothOwner?):BoothOwnerDto?
    fun mapBoothOwnerDto(boothOwners:List<BoothOwner>):List<BoothOwnerDto>

    @InheritInverseConfiguration
    fun mapBoothOwner(user:displayUser):BoothOwnerDtoNoVisitor

    fun mapVisitor(visitor: Visitor?):VisitorDto?
    fun mapVisitor(visitors: List<Visitor>):List<VisitorDto>

    @InheritInverseConfiguration
    fun mapVisitor(visitor: VisitorDto?):Visitor

}