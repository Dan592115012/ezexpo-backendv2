package com.example.ezexpo.config

import com.example.ezexpo.entity.*
import com.example.ezexpo.repositoy.*
import com.example.ezexpo.repositoy.BoothOwnerRepository
import com.example.ezexpo.repositoy.VisitorRepository
import com.example.ezexpo.security.entity.Authority
import com.example.ezexpo.security.entity.AuthorityName
import com.example.ezexpo.security.entity.JwtUser
import com.example.ezexpo.security.repository.AuthorityRepository
import com.example.ezexpo.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.sql.Timestamp
import org.springframework.stereotype.Component
import javax.transaction.Transactional


@Component
class ApplicationLoader : ApplicationRunner {

    @Autowired
    lateinit var boothOwnerRepository: BoothOwnerRepository
    @Autowired
    lateinit var visitorRepository: VisitorRepository
    @Autowired
    lateinit var userRepositoryObj: UserRepositoryObj
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var jwtUserRepository: UserRepository
    @Autowired
    lateinit var eventOrganizerRepository: EventOrganizerRepository
    @Autowired
    lateinit var surveyQuestionRepository: SurveyQuestionRepository
    @Autowired
    lateinit var surveyAnswerRepository: SurveyAnswerRepository


    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_BOOTHOWNER)
        val auth2 = Authority(name = AuthorityName.ROLE_EVENTORGANIZER)

        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        val encoder = BCryptPasswordEncoder()
        val userTest = User("abc","abc","a@b.com")
        val userJwt = JwtUser(
                username = "abc",
                password = encoder.encode("123456"),
                email = userTest.email,
                enabled = true,
                firstname = userTest.firstName,
                lastname = userTest.lastName

        )
        jwtUserRepository.save(userJwt)
        userRepositoryObj.save(userTest)
        userTest.jwtUser = userJwt
        userJwt.user = userTest
        userJwt.authorities.add(auth1)
        userJwt.authorities.add(auth2)


    }

    @Transactional
    override fun run(args: ApplicationArguments?) {
        val encoder = BCryptPasswordEncoder()
        val auth1 = Authority(name = AuthorityName.ROLE_BOOTHOWNER)
        val auth2 = Authority(name = AuthorityName.ROLE_EVENTORGANIZER)

        authorityRepository.save(auth1)
        authorityRepository.save(auth2)

        var emoTime1 = visitorRepository.save(Visitor(5, 6, 8, 3, 7, 5, 10, 20, 24, Timestamp(434762).time))
        var emoTime2 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 10, 23, Timestamp(434764).time))
        var emoTime3 = visitorRepository.save(Visitor(3, 4, 1, 2, 7, 1, 12, 14, 16, Timestamp(434766).time))
        var emoTime4 = visitorRepository.save(Visitor(5, 1, 3, 1, 1, 2, 15, 14, 14, Timestamp(434768).time))
        var emoTime5 = visitorRepository.save(Visitor(4, 2, 5, 2, 1, 1, 15, 22, 8, Timestamp(434770).time))

        var emoTime6 = visitorRepository.save(Visitor(4, 3, 8, 2, 7, 5, 15, 19, 25, Timestamp(434762).time))
        var emoTime7 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 10, 23, Timestamp(434764).time))
        var emoTime8 = visitorRepository.save(Visitor(17, 1, 7, 3, 2, 1, 11, 19, 23, Timestamp(434766).time))
        var emoTime9 = visitorRepository.save(Visitor(5, 1, 3, 1, 1, 2, 17, 15, 15, Timestamp(434768).time))
        var emoTime10 = visitorRepository.save(Visitor(4, 2, 5, 2, 4, 1, 15, 22, 11, Timestamp(434770).time))

        var emoTime11 = visitorRepository.save(Visitor(8, 1, 10, 1, 3, 2, 12, 17, 20, Timestamp(434762).time))
        var emoTime12 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 10, 23, Timestamp(434764).time))
        var emoTime13 = visitorRepository.save(Visitor(12, 3, 6, 3, 4, 1, 11, 19, 21, Timestamp(434766).time))
        var emoTime14 = visitorRepository.save(Visitor(5, 1, 3, 1, 1, 2, 17, 15, 15, Timestamp(434768).time))
        var emoTime15 = visitorRepository.save(Visitor(4, 2, 5, 6, 5, 1, 20, 22, 21, Timestamp(434770).time))

        var emoTime16 = visitorRepository.save(Visitor(4, 3, 8, 2, 7, 5, 15, 19, 25, Timestamp(434762).time))
        var emoTime17 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 10, 23, Timestamp(434764).time))
        var emoTime18 = visitorRepository.save(Visitor(15, 1, 7, 3, 2, 1, 11, 19, 21, Timestamp(434766).time))
        var emoTime19 = visitorRepository.save(Visitor(7, 2, 5, 1, 2, 1, 21, 15, 24, Timestamp(434768).time))
        var emoTime20 = visitorRepository.save(Visitor(4, 2, 5, 2, 4, 1, 15, 22, 11, Timestamp(434770).time))

        var emoTime21 = visitorRepository.save(Visitor(6, 1, 7, 2, 5, 2, 20, 19, 24, Timestamp(434762).time))
        var emoTime22 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 10, 23, Timestamp(434764).time))
        var emoTime23 = visitorRepository.save(Visitor(17, 1, 7, 3, 2, 1, 11, 19, 23, Timestamp(434766).time))
        var emoTime24 = visitorRepository.save(Visitor(5, 1, 3, 1, 1, 2, 17, 15, 15, Timestamp(434768).time))
        var emoTime25 = visitorRepository.save(Visitor(4, 2, 5, 2, 4, 1, 15, 22, 11, Timestamp(434770).time))

        var survey1 = surveyQuestionRepository.save(SurveyQuestion("Test Survey1","Description for survetTest1","What is your favorite product ?","Why did you purchase this product ?","How satisfied are you with this exhibition ?","Would you recommend this exhibition to a friend?","question5"))
        var survey2 = surveyQuestionRepository.save(SurveyQuestion("Test Survey2","Description for survetTest2","question21","question22","question23","question24","question25"))
        var survey3 = surveyQuestionRepository.save(SurveyQuestion("Test Survey3","Description for survetTest3","question31","question32","question33","question34","question35"))
        var survey4 = surveyQuestionRepository.save(SurveyQuestion("Test Survey4","Description for survetTest4","question41","question42","question43","question44","question45"))
        var survey5 = surveyQuestionRepository.save(SurveyQuestion("Test Survey5","Description for survetTest5","question51","question52","question53","question54","question55"))

        var answer1 = surveyAnswerRepository.save(SurveyAnswer(3,4,3,4,2,null))
        var answer2 = surveyAnswerRepository.save(SurveyAnswer(3,5,3,1,2,"this expo very bored u have to have some entertainment in expo"))
        var answer3 = surveyAnswerRepository.save(SurveyAnswer(1,2,1,4,5,"i like so much, how can i know when have expo like this again"))
        var answer4 = surveyAnswerRepository.save(SurveyAnswer(4,3,2,2,5,"next time can we allow pet in expo ?"))
        var answer5 = surveyAnswerRepository.save(SurveyAnswer(4,1,4,2,3,"event on stage is too much loud canu turn volume down next time?"))

        var answer6 = surveyAnswerRepository.save(SurveyAnswer(3,4,3,4,2,"good"))
        var answer7 = surveyAnswerRepository.save(SurveyAnswer(3,5,3,1,2,"good"))
        var answer8 = surveyAnswerRepository.save(SurveyAnswer(1,2,1,4,5,"good"))
        var answer9= surveyAnswerRepository.save(SurveyAnswer(4,3,2,2,5,"good"))
        var answer10 = surveyAnswerRepository.save(SurveyAnswer(4,1,4,2,3,"good"))

        var boothOwn1 = boothOwnerRepository.save(BoothOwner("Prayuth", "Junungkarn", "prayuth@gmail.com", "prayuth123", "1212312121", UserRole.BOOTHOWNER, "Submarine booth"))
        val userJwt1 = JwtUser(
                username = boothOwn1.username,
                password = encoder.encode(boothOwn1.password),
                email = boothOwn1.email,
                enabled = true,
                firstname = boothOwn1.firstName,
                lastname = boothOwn1.lastName

        )
        userRepositoryObj.save(boothOwn1)
        boothOwn1.jwtUser = userJwt1
        userJwt1.user = boothOwn1
        jwtUserRepository.save(userJwt1)
        userJwt1.authorities.add(auth1)
        var boothOwn2 = boothOwnerRepository.save(BoothOwner("Nattapat", "Tamtrakool", "nattapat@gmail.com", "nattapat123", "123456", UserRole.BOOTHOWNER, "Dan Booth"))
        val userJwt2 = JwtUser(
                username = boothOwn2.username,
                password = encoder.encode(boothOwn2.password),
                email = boothOwn2.email,
                enabled = true,
                firstname = boothOwn2.firstName,
                lastname = boothOwn2.lastName

        )
        userRepositoryObj.save(boothOwn2)
        boothOwn2.jwtUser = userJwt2
        userJwt2.user = boothOwn2
        jwtUserRepository.save(userJwt2)
        userJwt2.authorities.add(auth1)
        var boothOwn3 = boothOwnerRepository.save(BoothOwner("Tony", "Stark", "tony@gmail.com", "tony123", "123456", UserRole.BOOTHOWNER, "Ironman Booth"))
        val userJwt3 = JwtUser(
                username = boothOwn3.username,
                password = encoder.encode(boothOwn3.password),
                email = boothOwn3.email,
                enabled = true,
                firstname = boothOwn3.firstName,
                lastname = boothOwn3.lastName

        )
        userRepositoryObj.save(boothOwn3)
        boothOwn3.jwtUser = userJwt3
        userJwt3.user = boothOwn3
        jwtUserRepository.save(userJwt3)
        userJwt3.authorities.add(auth1)
        var boothOwn4 = boothOwnerRepository.save(BoothOwner("Steve", "Roger", "steve@gmail.com", "steve123", "123456", UserRole.BOOTHOWNER, "Sheild Booth"))
        val userJwt4 = JwtUser(
                username = boothOwn4.username,
                password = encoder.encode(boothOwn4.password),
                email = boothOwn4.email,
                enabled = true,
                firstname = boothOwn4.firstName,
                lastname = boothOwn4.lastName

        )

        userRepositoryObj.save(boothOwn4)
        boothOwn4.jwtUser = userJwt4
        userJwt4.user = boothOwn4
        jwtUserRepository.save(userJwt4)
        userJwt4.authorities.add(auth1)

        var boothOwn5 = boothOwnerRepository.save(BoothOwner("Thor", "Asgard", "thor@gmail.com", "thor123", "123456", UserRole.BOOTHOWNER, "Hammer Booth"))
        val userJwt5 = JwtUser(
                username = boothOwn5.username,
                password = encoder.encode(boothOwn5.password),
                email = boothOwn5.email,
                enabled = true,
                firstname = boothOwn5.firstName,
                lastname = boothOwn5.lastName

        )
        userRepositoryObj.save(boothOwn5)
        boothOwn5.jwtUser = userJwt5
        userJwt5.user = boothOwn5
        jwtUserRepository.save(userJwt5)
        userJwt5.authorities.add(auth1)

        var eventOr1 = eventOrganizerRepository.save(EventOrganizer("Abdul","Alan","Abdul@gmail.com","abdul123","123456",UserRole.EVENTORGANIZER))
        val userJwt6 = JwtUser(
                username = eventOr1.username,
                password = encoder.encode(eventOr1.password),
                email = eventOr1.email,
                enabled = true,
                firstname = eventOr1.firstName,
                lastname = eventOr1.lastName

        )
        userRepositoryObj.save(eventOr1)
        eventOr1.jwtUser = userJwt6
        userJwt6.user = eventOr1
        jwtUserRepository.save(userJwt6)
        userJwt6.authorities.add(auth2)
//        boothOwn1.visitor = mutableListOf()


        boothOwn1.visitor?.add(emoTime1)
        boothOwn1.visitor?.add(emoTime2)
        boothOwn1.visitor?.add(emoTime3)
        boothOwn1.visitor?.add(emoTime4)
        boothOwn1.visitor?.add(emoTime5)

        boothOwn2.visitor?.add(emoTime6)
        boothOwn2.visitor?.add(emoTime7)
        boothOwn2.visitor?.add(emoTime8)
        boothOwn2.visitor?.add(emoTime9)
        boothOwn2.visitor?.add(emoTime10)

        boothOwn3.visitor?.add(emoTime11)
        boothOwn3.visitor?.add(emoTime12)
        boothOwn3.visitor?.add(emoTime13)
        boothOwn3.visitor?.add(emoTime14)
        boothOwn3.visitor?.add(emoTime15)

        boothOwn4.visitor?.add(emoTime16)
        boothOwn4.visitor?.add(emoTime17)
        boothOwn4.visitor?.add(emoTime18)
        boothOwn4.visitor?.add(emoTime19)
        boothOwn4.visitor?.add(emoTime20)

        boothOwn5.visitor?.add(emoTime21)
        boothOwn5.visitor?.add(emoTime22)
        boothOwn5.visitor?.add(emoTime23)
        boothOwn5.visitor?.add(emoTime24)
        boothOwn5.visitor?.add(emoTime25)

        eventOr1.surveyQuestion?.add(survey1)
        survey1.surveyAnswer?.add(answer1)
        survey1.surveyAnswer?.add(answer2)
        survey1.surveyAnswer?.add(answer3)
        survey1.surveyAnswer?.add(answer4)
        survey1.surveyAnswer?.add(answer5)

        survey2.surveyAnswer?.add(answer6)
        survey2.surveyAnswer?.add(answer7)
        survey2.surveyAnswer?.add(answer8)
        survey2.surveyAnswer?.add(answer9)
        survey2.surveyAnswer?.add(answer10)

//        eventOr1.surveyQuestion?.add(survey2)
//        survey2.surveyAnswer?.add(answer1)
//        survey2.surveyAnswer?.add(answer2)
//
//        eventOr1.surveyQuestion?.add(survey3)
//        survey3.surveyAnswer?.add(answer1)
//        survey3.surveyAnswer?.add(answer2)
//
//        eventOr1.surveyQuestion?.add(survey4)
//        survey4.surveyAnswer?.add(answer1)
//        survey4.surveyAnswer?.add(answer2)
//
//        eventOr1.surveyQuestion?.add(survey5)
//        survey5.surveyAnswer?.add(answer1)
//        survey5.surveyAnswer?.add(answer2)



      //  loadUsernameAndPassword()
    }

}

