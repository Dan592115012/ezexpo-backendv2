package com.example.ezexpo.repositoy

import com.example.ezexpo.entity.SurveyQuestion
import org.springframework.data.repository.CrudRepository

interface SurveyQuestionRepository  : CrudRepository<SurveyQuestion, Long> {



}