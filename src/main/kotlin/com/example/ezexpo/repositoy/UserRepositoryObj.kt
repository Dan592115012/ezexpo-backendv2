package com.example.ezexpo.repositoy

import com.example.ezexpo.entity.User
import com.example.ezexpo.entity.UserRole
import org.springframework.data.repository.CrudRepository
import java.lang.Enum

interface UserRepositoryObj : CrudRepository<User, Long> {
    fun getUserByUsernameAndPassword(username: String, password: String): User?
    fun getUserByEmail(email: String): User?
    fun findByUserRole(userRole: UserRole):List<User>

//    fun findUserByRole(userRole: Enum<UserRole>): List<User>

}