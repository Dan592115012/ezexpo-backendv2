package com.example.ezexpo.repositoy

import com.example.ezexpo.entity.Visitor
import org.springframework.data.repository.CrudRepository

interface VisitorRepository : CrudRepository<Visitor,Long> {


}