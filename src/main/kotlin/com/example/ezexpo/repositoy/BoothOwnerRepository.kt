package com.example.ezexpo.repositoy
import com.example.ezexpo.entity.BoothOwner
import org.springframework.data.repository.CrudRepository
import com.example.ezexpo.entity.UserRole


interface BoothOwnerRepository : CrudRepository<BoothOwner,Long> {
     fun findByBoothName(boothName: String): BoothOwner?
    //fun findByName(boothName: String): BoothOwner?


}