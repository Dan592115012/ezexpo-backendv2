package com.example.ezexpo.service

import com.example.ezexpo.entity.SurveyAnswer
import com.example.ezexpo.entity.SurveyQuestion

interface SurveyQuestionService {
    fun getAllSurvey(): List<SurveyQuestion>
    fun getSurveybyId(id:Long): SurveyQuestion?
    fun save(survey: SurveyQuestion): SurveyQuestion


}