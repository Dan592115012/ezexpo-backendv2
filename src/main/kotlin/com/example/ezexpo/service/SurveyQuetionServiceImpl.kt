package com.example.ezexpo.service

import com.example.ezexpo.dao.SurveyQuestionDao
import com.example.ezexpo.entity.SurveyQuestion
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SurveyQuetionServiceImpl : SurveyQuestionService {



    @Autowired
    lateinit var surveyQuestionDao: SurveyQuestionDao
    override fun getAllSurvey(): List<SurveyQuestion> {
        return  surveyQuestionDao.getAllSurvey()
    }

    override fun getSurveybyId(id: Long): SurveyQuestion? {
        return surveyQuestionDao.getSurveyById(id)
    }

    override fun save(survey: SurveyQuestion): SurveyQuestion {
        return surveyQuestionDao.save(survey)
    }

}