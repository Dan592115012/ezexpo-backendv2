pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}
rootProject.name = "ezexpo"
rootProject.buildFileName = "build.gradle.kts"
