import org.jetbrains.kotlin.allopen.gradle.AllOpenExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.1.6.RELEASE"
    id("io.spring.dependency-management") version "1.0.7.RELEASE"
    kotlin("jvm") version "1.2.71"
    kotlin("plugin.spring") version "1.2.71"
    id("org.jetbrains.kotlin.plugin.jpa") version "1.2.71"
}

apply{
    plugin("org.jetbrains.kotlin.plugin.jpa")
    plugin("org.jetbrains.kotlin.plugin.allopen")
    plugin("kotlin-kapt")
}

configure<AllOpenExtension>{
    annotation("javax.persistence.Entity")
}


group = "com.example"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    mavenCentral()
    maven { setUrl ("https://repo.spring.io/milestone") }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    compile ("org.springframework.boot:spring-boot-starter-web")
    compile("org.springframework.boot:spring-boot-starter-data-jpa")
    compile("mysql:mysql-connector-java:8.0.15")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    compile("org.mapstruct:mapstruct:1.3.0.Final")
    compile("org.mapstruct:mapstruct-jdk8:1.3.0.Final")
    compile("javax.xml.bind:jaxb-api:2.3.0")
    compile("com.amazonaws:aws-java-sdk:1.11.520")
    runtime("com.h2database:h2")

    compile("org.springframework.boot:spring-boot-devtools")
    kapt("org.mapstruct:mapstruct-processor:1.3.0.Final")

    compile ("org.springframework.boot:spring-boot-starter-security")
    compile("org.springframework.mobile:spring-mobile-device:2.0.0.M3")
    compile("io.jsonwebtoken:jjwt:0.9.1")

}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}
